import { Pipe, PipeTransform } from '@angular/core';
import { makeTitle } from '../utility/slugify';

@Pipe({
  name: 'unslugify',
})
export class UnslugifyPipe implements PipeTransform {
  constructor() {}

  transform(v: string): string {
    return makeTitle(v);
  }
}
