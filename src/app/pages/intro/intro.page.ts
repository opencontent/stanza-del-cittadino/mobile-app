import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';
import { environment } from '../../../environments/environment';
import SwiperCore, { Navigation, Pagination, SwiperOptions, Virtual } from 'swiper';
import { SwiperComponent } from 'swiper/angular';
// install Swiper modules
SwiperCore.use([Virtual]);
@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {


  appName: any = environment.appName;
  config: SwiperOptions = {
    modules: [Navigation, Pagination],
    slidesPerView: 3,
    spaceBetween: 50,
    navigation: true,
    pagination: { clickable: true },
    scrollbar: { draggable: true },
  };
  constructor(private router: Router, private storage: StorageService) {}

  @ViewChild('swiper', { static: false }) swiper?: SwiperComponent;
  slideNext(){
    this.swiper.swiperRef.slideNext(100);
  }
  ngOnInit() {}

  start() {
    this.storage
      .set('INTRO_SEEN', 'true')
      .then(() => {
        this.router.navigateByUrl('/login', { replaceUrl: true });
      })
      .catch((err) => {
        console.log('err', err);
      });
  }
}
