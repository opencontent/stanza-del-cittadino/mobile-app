import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PraticeDetailsPage } from './pratice-details.page';

const routes: Routes = [
  {
    path: '',
    component: PraticeDetailsPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PraticeDetailsPageRoutingModule {}
