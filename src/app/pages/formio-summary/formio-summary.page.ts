import { Component, OnDestroy, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationsService } from '../../services/applications.service';
import { FormioBaseComponent } from '@formio/angular';
import { ServicesService } from '../../services/services.service';
import { StorageService } from '../../services/storage.service';
import { UserService } from '../../services/user.service';
import { User } from '../../interfaces/user';
import { ApplicationService } from '../../interfaces/services';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-formio-summary',
  templateUrl: './formio-summary.page.html',
  styleUrls: ['./formio-summary.page.scss'],
})
export class FormioSummaryPage implements OnInit, OnDestroy {
  urlForm: string;
  service: any;
  data: any = {};
  applicantForm: any;
  showSubmit: boolean;
  showPrevious: boolean;
  showNext: boolean;
  showCancel: boolean;
  steps: any[] = [];
  activeStep = 1;
  stepTitle: any;
  formioReference: FormioBaseComponent;
  user: User;
  private unsubscribe$ = new Subject<void>();

  constructor(
    public loadingController: LoadingController,
    private router: Router,
    private route: ActivatedRoute,
    private applicationsService: ApplicationsService,
    public alertController: AlertController,
    private servicesService: ServicesService,
    private storageService: StorageService,
    private userService: UserService,
    private translateService: TranslateService
  ) {
    this.route.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.service = this.router.getCurrentNavigation().extras.state.detailPost;
        this.data = {
          data: {
            ...this.service.data,
          },
        };
        this.urlForm = this.service.flow_steps[0].parameters.url + this.service.flow_steps[0].parameters.formio_id;
        this.applicationsService
          .getFormIOService(this.urlForm)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe((res) => {
            res.display = 'form';
            this.applicantForm = res;
          });
      }
    });
  }

  ionViewWillEnter() {
    this.storageService
      .decodeUser()
      .then((user) => {
        if (user) {
          this.userService
            .getUser(user.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(async (res) => {
              this.user = res;
            });
        }
      })
      .catch((err) => {
        console.error(err);
      });
  }

  ngOnInit() {}

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: '',
      message: this.translateService.instant('common.recupero_dati'),
      duration: 1000,
    });
    await loading.present();
  }

  submitForm() {
    const applicantData: ApplicationService = {
      user: this.user.id,
      service: this.service.id,
      data: {
        ...this.service.data,
      },
      status: '1900',
    };
    this.applicationsService
      .postApplications(applicantData)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (res) => {
          this.router.navigate(['/formio-complete'], { state: { applicationsData: res } });
        },
        (error) => {
          this.presentAlertError(error);
        }
      );
  }

  async presentAlertConfirmSubmit() {
    const alert = await this.alertController.create({
      cssClass: '',
      header: this.translateService.instant('formio.domanda_invia_pratica'),
      message: '',
      buttons: [
        {
          text: this.translateService.instant('common.no'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {},
        },
        {
          text: this.translateService.instant('common.si_sono_sicuro'),
          handler: () => {
            this.submitForm();
          },
        },
      ],
    });

    await alert.present();
  }

  async presentAlertError(error: any) {
    const alert = await this.alertController.create({
      cssClass: '',
      header: error.error.title || 'Ops',
      message: error.error.description || this.translateService.instant('error.errore_invia_pratica'),
      buttons: [
        {
          text: this.translateService.instant('common.chiudi'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {},
        },
      ],
    });

    await alert.present();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
