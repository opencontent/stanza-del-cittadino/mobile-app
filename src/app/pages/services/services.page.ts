import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { IonInfiniteScroll, ModalController } from '@ionic/angular';
import { Subject } from 'rxjs';
import { ServicesService } from '../../services/services.service';
import { HelpPagePage } from '../../modals/help-page/help-page.page';

@Component({
  selector: 'app-services',
  templateUrl: './services.page.html',
  styleUrls: ['./services.page.scss'],
})
export class ServicesPage implements OnInit, OnDestroy {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  next: string;
  services: any[] = [];
  isLoading = false;
  private unsubscribe$ = new Subject<void>();
  category: any;
  servicesByCategory: any;
  group: any;
  hasCategory: boolean;
  hasGroup: boolean;
  constructor(
    private serviceReport: ServicesService,
    private router: Router,
    private route: ActivatedRoute,
    public modalController: ModalController
  ) {
    this.route.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state) {
        if (this.router.getCurrentNavigation().extras.state.data) {
          this.category = this.router.getCurrentNavigation().extras.state.data.category;
          if (this.category) {
            this.hasCategory = true;
          }
          this.services = this.router.getCurrentNavigation().extras.state.data.data;
          this.group = this.router.getCurrentNavigation().extras.state.data.group;
          if (this.group) {
            this.hasGroup = true;
            this.loadServices(this.category, this.group);
          }
        }
      } else if (this.route.snapshot.queryParams) {
        if (this.route.snapshot.params) {
          if (!this.services) {
            this.category = this.route.snapshot.params.topics_id || '';
            this.loadServices(this.category, this.group);
          }
        }
      }
    });
  }

  ngOnInit() {}

  loadServices(filterCategory: any, filterGroup: any) {
    this.isLoading = true;
    this.serviceReport
      .getAllServices(filterCategory?.id, filterGroup?.id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (res) => {
          this.services = res;
          this.next = res.next;
        },
        (error) => {},
        () => {
          this.isLoading = false;
        }
      );
  }

  loadData(event) {
    setTimeout(() => {
      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.next === null) {
        event.target.disabled = true;
        this.infiniteScroll.disabled = true;
      } else {
        event.target.complete();
        this.serviceReport
          .getServiceByUrl(this.next)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(
            (res) => {
              this.next = res.next;
              this.services = [...this.services, ...res.items];
            },
            (error) => {},
            () => {
              this.isLoading = false;
            }
          );
      }
    }, 500);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  openDetails(url: string, data) {
    const navigationExtras: NavigationExtras = {
      state: {
        detailPost: data,
      },
    };
    this.router.navigate([url], navigationExtras);
  }

  async openDescriptionModal() {
    const modal = await this.modalController.create({
      component: HelpPagePage,
      cssClass: '',
      componentProps: {
        service: null,
      },
    });
    return await modal.present();
  }
}
