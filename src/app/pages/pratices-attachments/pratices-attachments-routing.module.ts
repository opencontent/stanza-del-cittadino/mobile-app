import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PraticesAttachmentsPage } from './pratices-attachments.page';

const routes: Routes = [
  {
    path: '',
    component: PraticesAttachmentsPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PraticesAttachmentsPageRoutingModule {}
