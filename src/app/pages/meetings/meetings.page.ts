import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationsService } from '../../services/applications.service';
import { MeetingsService } from '../../services/meetings.service';

@Component({
  selector: 'app-meetings',
  templateUrl: './meetings.page.html',
  styleUrls: ['./meetings.page.scss'],
})
export class MeetingsPage implements OnInit {
  meetings: any[] = [];
  private meetingsIds: any[];

  constructor(
    private route: ActivatedRoute,
    private applicationsService: ApplicationsService,
    private router: Router,
    private meetingsService: MeetingsService
  ) {
    this.route.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.meetingsIds = this.router.getCurrentNavigation().extras.state.detailPost.meetings;
        this.meetingsIds.forEach((id) => {
          this.meetingsService.getMeetingByID(id).subscribe((res) => {
            this.meetings.push(res);
          });
        });
      }
    });
  }

  ngOnInit() {}
}
