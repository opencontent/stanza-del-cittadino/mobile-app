import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubTopicsPageRoutingModule } from './sub-topics-routing.module';

import { SubTopicsPage } from './sub-topics.page';
import { TranslateModule } from '@ngx-translate/core';
import { SharedComponentsModule } from '../../components/shared-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SubTopicsPageRoutingModule,
    TranslateModule,
    SharedComponentsModule,
  ],
  declarations: [SubTopicsPage],
})
export class SubTopicsPageModule {}
