import { Component, OnInit } from '@angular/core';
import { FingerprintAIO, FingerprintOptions } from '@ionic-native/fingerprint-aio/ngx';
import { TranslateService } from '@ngx-translate/core';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'app-security',
  templateUrl: './security.page.html',
  styleUrls: ['./security.page.scss'],
})
export class SecurityPage implements OnInit {
  isChecked: boolean;
  value = false;
  isAvailable = false;

  constructor(
    public faio: FingerprintAIO,
    private storage: StorageService,
    private translateService: TranslateService
  ) {}

  showFingerPrint(evt) {
    const options: FingerprintOptions = {
      title: this.translateService.instant('sicurezza.autenticazione_biometrica'),
      subtitle: '',
      description: this.translateService.instant('sicurezza.autenticazione_biometrica_descrizione'),
      fallbackButtonTitle: this.translateService.instant('sicurezza.usa_codice'),
      cancelButtonTitle: this.translateService.instant('common.chiudi'),
      disableBackup: false,
    };
    this.faio.isAvailable().then(
      (isAvailable) => {
        if (isAvailable === 'finger' || isAvailable === 'face' || isAvailable === 'biometric') {
          this.isAvailable = true;
          this.faio
            .show(options)
            .then(() => {
              this.value = evt;
              this.storage.remove('BIOMETRIC_ACCESS').then(() => {
                this.storage
                  .setBiometrics(this.value.toString())
                  .then(() => {})
                  .catch((err) => {
                    console.log(err);
                  });
              });
            })
            .catch((e) => {
              this.value = this.value;
              console.log('Inside failure on cancel');
            });
        }
      },
      (error) => {
        alert(error.message);
        console.log('isAvailable error', error);
      }
    );
  }

  ngOnInit() {}

  ionViewWillEnter() {
    this.faio
      .isAvailable()
      .then((isAvailable) => {
        if (isAvailable === 'finger' || isAvailable === 'face' || isAvailable === 'biometric') {
          this.isAvailable = true;
          this.getStorageBiometric();
        }
      })
      .catch((err) => {
        alert(err.message);
      });
  }

  async getStorageBiometric() {
    await this.storage.getKeys().then((res) => {
      if (res) {
        if (res.value.includes('BIOMETRIC_ACCESS')) {
          this.storage.get('BIOMETRIC_ACCESS').then((res) => {
            if (res) {
              // Cast value string in boolean
              const tmp = res.value === 'true';
              setTimeout(() => {
                this.value = tmp;
              }, 0);
            }
          });
        }
      }
    });
  }

  change(evt, value) {
    if (evt.checked == value) {
      return;
    }
    this.showFingerPrint(evt.checked);
  }
}
