import { Component, OnInit } from '@angular/core';
import { User } from '../../../interfaces/user';
import { UserService } from '../../../services/user.service';
import { StorageService } from '../../../services/storage.service';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-domicile',
  templateUrl: './domicile.page.html',
  styleUrls: ['./domicile.page.scss'],
})
export class DomicilePage implements OnInit {
  private loading: HTMLIonLoadingElement;
  user: User;
  enableEdit = false;
  profileForm: UntypedFormGroup;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private userService: UserService,
    private storageService: StorageService,
    public alertController: AlertController,
    public loadingController: LoadingController,
    public formBuilder: UntypedFormBuilder,
    public toastController: ToastController,
    public router: Router,
    private translateService: TranslateService
  ) {
    this.profileForm = this.formBuilder.group({
      indirizzo_domicilio: [
        {
          value: null,
        },
      ],
      cap_domicilio: [
        {
          value: null,
        },
      ],
      citta_domicilio: [
        {
          value: null,
        },
      ],
      provincia_domicilio: [
        {
          value: null,
        },
      ],
      stato_domicilio: [
        {
          value: null,
        },
      ],
    });
  }

  ngOnInit() {
    this.presentLoading().then(() => {
      this.storageService
        .decodeUser()
        .then((user) => {
          this.userService
            .getUser(user.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
              async (res) => {
                this.user = res;
                this.profileForm.patchValue({
                  indirizzo_domicilio: this.user.indirizzo_domicilio,
                  cap_domicilio: this.user.cap_domicilio,
                  citta_domicilio: this.user.citta_domicilio,
                  provincia_domicilio: this.user.provincia_domicilio,
                  stato_domicilio: this.user.stato_domicilio,
                });
                this.loading.dismiss();
              },
              (err) => {
                this.loading.dismiss();
              }
            );
        })
        .catch((err) => {
          this.loading.dismiss();
          this.router.navigate(['error-page']);
        });
    });
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      cssClass: '',
      message: this.translateService.instant('messaggi.dati_aggiornati'),
    });
    await this.loading.present();
  }

  edit() {
    this.enableEdit = !this.enableEdit;
  }

  onSubmit() {
    this.user = {
      ...this.user,
      indirizzo_domicilio: this.profileForm.get('indirizzo_domicilio').value,
      cap_domicilio: this.profileForm.get('cap_domicilio').value,
      citta_domicilio: this.profileForm.get('citta_domicilio').value,
      provincia_domicilio: this.profileForm.get('provincia_domicilio').value,
      stato_domicilio: this.profileForm.get('stato_domicilio').value,
    };
    this.profileForm.patchValue(this.user);
    this.userService.patchUser(this.user.id, this.user).subscribe(
      (res) => {
        this.presentToast();
      },
      (error) => {
        this.presentAlertError(error);
      }
    );
  }

  get errorControl() {
    return this.profileForm.controls;
  }

  submitForm() {}

  async presentAlertError(error: any) {
    const alert = await this.alertController.create({
      cssClass: '',
      header: error.error.title || 'Ops',
      message: error.error.description || 'Si sono verificati errori durante il processo di invio',
      buttons: [
        {
          text: this.translateService.instant('common.chiudi'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {},
        },
      ],
    });

    await alert.present();
  }

  async presentAlertSuccess() {
    const alert = await this.alertController.create({
      cssClass: '',
      header: '',
      message: this.translateService.instant('messaggi.dati_aggiornati'),
      buttons: [
        {
          text: this.translateService.instant('common.chiudi'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {},
        },
      ],
    });

    await alert.present();
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: this.translateService.instant('messaggi.dati_aggiornati'),
      duration: 2000,
    });
    toast.present();
  }
}
