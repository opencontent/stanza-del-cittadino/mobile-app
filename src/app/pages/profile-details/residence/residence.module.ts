import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResidencePageRoutingModule } from './residence-routing.module';

import { ResidencePage } from './residence.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, ResidencePageRoutingModule, ReactiveFormsModule, TranslateModule],
  declarations: [ResidencePage],
})
export class ResidencePageModule {}
