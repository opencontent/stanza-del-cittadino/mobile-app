import { Component, OnDestroy, OnInit } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { ServicesService } from '../../services/services.service';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { forkJoin, Subject } from 'rxjs';
import { CategoriesService } from '../../services/categories.service';
import { HelpPagePage } from '../../modals/help-page/help-page.page';
import { MenuController, ModalController } from '@ionic/angular';
import _, { groupBy } from 'lodash';
import { Categories } from '../../interfaces/categories';
import { RecipientsService } from '../../services/recipients.service';
import { GeographicAreasService } from '../../services/geographic-areas.service';
import { StorageService } from '../../services/storage.service';
import { concatGroups } from '../../utility/grouped';

@Component({
  selector: 'app-topics',
  templateUrl: './topics.page.html',
  styleUrls: ['./topics.page.scss'],
})
export class TopicsPage implements OnInit, OnDestroy {
  listServices: any[] = [];
  isLoading = true;
  type = 'categories';
  servicesByCategory: any[] = [];
  servicesByGroup: { group?: any; grouped?: any }[] = [];
  categories: Categories[];
  private unsubscribe$ = new Subject<void>();
  private groups: any;
  facets: any;
  urlFilterParams = '';
  searchTextActive: number;
  services: any;

  searchTerm: string;
  enableButtonSearch: boolean;

  constructor(
    private servicesService: ServicesService,
    private router: Router,
    private route: ActivatedRoute,
    private categoriesService: CategoriesService,
    public modalController: ModalController,
    private menu: MenuController,
    private recipientsService: RecipientsService,
    private geographicAreasServices: GeographicAreasService,
    private storage: StorageService
  ) {}

  ionViewWillEnter() {
    forkJoin([
      this.categoriesService.getCategories('true'),
      this.servicesService.getGroups(null, null, null, 'true'),
      this.servicesService.getFacets(),
    ])
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((results) => {
        this.categories = results[0];
        this.groups = results[1];
        this.facets = results[2];
        this.getStorageFilters();
      });
  }

  ngOnInit() {}

  getStorageFilters() {
    this.storage.getKeys().then((res) => {
      if (res) {
        if (res.value.includes('FILTER_SERVICES') && res.value.includes('FILTER_SERVICES_PARAMS')) {
          this.storage.get('FILTER_SERVICES').then((res) => {
            if (res) {
              const dataStorageFacets = JSON.parse(res.value);
              if (
                Object.keys(dataStorageFacets.geographic_area_id).length !==
                  Object.keys(this.facets.geographic_area_id).length ||
                Object.keys(dataStorageFacets.recipient_id).length !== Object.keys(this.facets.recipient_id).length ||
                Object.keys(dataStorageFacets.topics_id).length !== Object.keys(this.facets.topics_id).length
              ) {
                return this.facets;
              } else {
                this.facets = dataStorageFacets;
              }
            }
          });
          this.storage.get('FILTER_SERVICES_PARAMS').then((res) => {
            if (res) {
              this.urlFilterParams = res.value;
              this.getServicesUrlParams(this.urlFilterParams);
            }
          });
        } else {
          this.getServicesUrlParams('');
        }
      }
    });
  }

  getServicesUrlParams(params: string) {
    this.isLoading = true;
    this.servicesService
      .getServicesUrlParameters(params)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (res) => {
          this.servicesByCategory = [];
          this.listServices = res.filter((s) => s.status == '1' || s.status == '4');
          const groupByTopics = groupBy(this.listServices, (n) => {
            return n.topics_id;
          });
          let parentCategories = [];
          for (const [key, value] of Object.entries(groupByTopics)) {
            const getCategoryTopic = _.filter(this.categories, (cat) => cat.id === key)[0];
            let getParentCategoryTopic;
            if (getCategoryTopic) {
              getParentCategoryTopic = _.filter(this.categories, (cat) => cat.id === getCategoryTopic.parent_id)[0];
              if (getParentCategoryTopic) {
                parentCategories.push(getParentCategoryTopic);
              } else {
                parentCategories.push(getCategoryTopic);
              }
            }
          }
          // Remove duplicate
          parentCategories = parentCategories.filter(
            (value, index, self) => index === self.findIndex((t) => t.slug === value.slug)
          );
          parentCategories.forEach((par_cat) => {
            const filteredGroups = _.filter(this.listServices, (ser) => ser.topics_id === par_cat.id);
            this.servicesByCategory.push({
              parent_topic: {
                name: par_cat.name,
                services: _.filter(
                  this.listServices,
                  (ser) => ser.topics_id === par_cat.id && ser.shared_with_group === false
                ),
                groups: concatGroups(filteredGroups, this.categories, this.groups),
                subCategories: [
                  ...this.categories.filter((cat) => cat.parent_id === par_cat.id),
                  /*  .map((cat) => {
                      cat.groups = this.groups.filter((g) => g.id === cat.parent_id);
                      return cat;
                    })*/
                ],
              },
            });
          });
          this.isLoading = false;
        },
        (err) => {
          this.isLoading = false;
        },
        () => {}
      );
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  navigateWithData(url: string, data) {
    const navigationExtras: NavigationExtras = {
      state: {
        data,
        paramsUrl: this.urlFilterParams,
      },
    };
    this.router.navigate([url], navigationExtras);
  }

  async openHelpModal() {
    const modal = await this.modalController.create({
      component: HelpPagePage,
      cssClass: '',
      componentProps: {
        service: null,
      },
    });
    return await modal.present();
  }

  searchService(event) {
    this.searchTerm = event.srcElement.value;
    if (!this.searchTerm || this.searchTerm.length < 3) {
      this.enableButtonSearch = false;
      return;
    } else {
      this.enableButtonSearch = true;
    }
  }

  onSearch() {
    const searchParams = new URLSearchParams(this.urlFilterParams);
    searchParams.set('q', this.searchTerm);
    this.urlFilterParams = searchParams.toString();
    this.isLoading = true;
    this.servicesService
      .getServicesUrlParameters(this.urlFilterParams)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (res) => {
          if (res.length) {
            this.servicesByCategory = [];
          }
          this.services = groupBy(res, (n) => {
            return n.topics;
          });
          this.searchTextActive = res.length;
        },
        (err) => {
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
  }

  clearSearch() {
    const searchParams = new URLSearchParams(this.urlFilterParams);
    searchParams.delete('q');
    this.urlFilterParams = searchParams.toString();
    this.services = [];
    this.searchTextActive = 0;
    this.getServicesUrlParams(this.urlFilterParams);
  }

  setValueFilter($event: any, categoryKey: string, value: string) {
    if (this.facets[categoryKey][value].hasOwnProperty('isChecked')) {
      this.facets[categoryKey][value].isChecked = !this.facets[categoryKey][value].isChecked;
    } else {
      this.facets[categoryKey][value].isChecked = true;
    }
    this.storage
      .set('FILTER_SERVICES', JSON.stringify(this.facets))
      .then(() => {})
      .catch((err) => {
        console.log(err);
      });
    const searchParams = new URLSearchParams(this.urlFilterParams);
    const tags = searchParams.getAll(`${categoryKey}[]`);
    if (!tags.includes(value)) {
      searchParams.append(`${categoryKey}[]`, value);
      this.urlFilterParams = this.fixedEncodeURI(searchParams.toString());
    } else {
      const tagsFiltered = searchParams.getAll(`${categoryKey}[]`).filter((tag) => tag !== value);
      searchParams.delete(`${categoryKey}[]`);
      for (const tag of tagsFiltered) searchParams.append(`${categoryKey}[]`, tag);
      this.urlFilterParams = this.fixedEncodeURI(searchParams.toString());
    }
    this.storage
      .set('FILTER_SERVICES_PARAMS', this.urlFilterParams)
      .then(() => {})
      .catch((err) => {
        console.log(err);
      });
    this.getServicesUrlParams(this.urlFilterParams);
  }

  fixedEncodeURI(str) {
    return encodeURI(str).replace(/%255B/g, '[').replace(/%255D/g, ']');
  }
}
