import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ApplicationsService } from '../../services/applications.service';
import { ApplicationDataConfig, ApplicationsConfig } from '../../interfaces/applications';
import { takeUntil } from 'rxjs/operators';
import { IonInfiniteScroll } from '@ionic/angular';
import { Subject } from 'rxjs';
import { NavigationExtras, Router } from '@angular/router';
import { getDateCalendar } from '../../utility/calendar';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-practices',
  templateUrl: './practices.page.html',
  styleUrls: ['./practices.page.scss'],
})
export class PracticesPage implements OnInit, OnDestroy {
  type: string;
  pratices: ApplicationsConfig;
  next: string;
  isLoading: boolean;
  private unsubscribe$ = new Subject<void>();

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  praticesPending: ApplicationsConfig;

  constructor(
    private applicationsService: ApplicationsService,
    private router: Router,
    private translateService: TranslateService
  ) {}

  ngOnInit() {
    this.type = 'highlights';
    this.applicationsService
      .getApplications('creationTime', '50', 'desc')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.pratices = res;
        this.pratices = {
          ...res,
          data: res.data.filter((pratice) => parseInt(pratice.status) >= 2000 && parseInt(pratice.status) < 20000),
        };
        this.praticesPending = {
          ...res,
          data: res.data.filter(
            (pratice) =>
              pratice.status === '1500' ||
              (pratice.status === '2000' && pratice.meetings.length) ||
              pratice.status === '4000' ||
              pratice.status === '4200' ||
              pratice.status === '4500'
          ),
        };

        this.praticesPending.data.map((item) => {
          if (item.data.calendar) {
            item.parse_data_calendar = getDateCalendar(
              item.data.calendar,
              this.translateService.getDefaultLang() || 'it'
            );
          }
        });
        this.next = res.links.next;
      });
  }

  segmentChanged(ev: any) {}

  loadData(event) {
    setTimeout(() => {
      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.next === null) {
        event.target.disabled = true;
        this.infiniteScroll.disabled = true;
      } else {
        event.target.complete();
        this.applicationsService
          .getApplicationsUrl(this.next)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(
            (res) => {
              res = {
                ...res,
                data: res.data.filter(
                  (pratice) => parseInt(pratice.status) >= 1900 && parseInt(pratice.status) < 20000
                ),
              };
              res.data.map((item) => {
                if (item.data.calendar) {
                  item.parse_data_calendar = getDateCalendar(
                    item.data.calendar,
                    this.translateService.getDefaultLang() || 'it'
                  );
                }
              });
              this.next = res.links.next;
              this.pratices.data = [...this.pratices.data, ...res.data];
            },
            (error) => {},
            () => {
              this.isLoading = false;
            }
          );
      }
    }, 500);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  openDetails(url: string, pratice: ApplicationDataConfig) {
    const navigationExtras: NavigationExtras = {
      state: {
        detailPost: pratice,
      },
    };
    this.router.navigate([url], navigationExtras);
  }
}
