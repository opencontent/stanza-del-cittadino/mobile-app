import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PracticesPageRoutingModule } from './practices-routing.module';

import { PracticesPage } from './practices.page';
import { SharedPipesModule } from '../../pipe/shared-pipes.module';
import { TranslateModule } from '@ngx-translate/core';
import { SharedComponentsModule } from 'src/app/components/shared-components.module';
import { SharedDirectivesModule } from 'src/app/directives/shared-directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PracticesPageRoutingModule,
    SharedPipesModule,
    TranslateModule,
    SharedDirectivesModule,
    SharedComponentsModule,
  ],
  declarations: [PracticesPage],
})
export class PracticesPageModule {}
