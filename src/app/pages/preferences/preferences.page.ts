import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'app-preferences',
  templateUrl: './preferences.page.html',
  styleUrls: ['./preferences.page.scss'],
})
export class PreferencesPage implements OnInit {
  language: string = this.translateService.getDefaultLang();
  constructor(private translateService: TranslateService, private storage: StorageService) {}

  ngOnInit() {
    this.storage.getKeys().then((res) => {
      if (res.value.includes('LANGUAGE')) {
        this.storage.get('LANGUAGE').then((res: any) => {
          if (res.value) {
            this.translateService.setDefaultLang(res.value);
            this.language = res.value;
          }
        });
      }
    });
  }

  setLanguage(lang: string) {
    this.storage.getKeys().then((res) => {
      if (res.value.includes('LANGUAGE')) {
        this.storage.remove('LANGUAGE').then(() => {
          this.storage
            .setLanguages(lang)
            .then(() => {
              this.translateService.use(lang);
              this.translateService.setDefaultLang(lang);
            })
            .catch((err) => {
              console.log(err);
            });
        });
      } else {
        this.storage
          .setLanguages(lang)
          .then(() => {
            this.translateService.use(lang);
            this.translateService.setDefaultLang(lang);
          })
          .catch((err) => {
            console.log(err);
          });
      }
    });
  }
}
