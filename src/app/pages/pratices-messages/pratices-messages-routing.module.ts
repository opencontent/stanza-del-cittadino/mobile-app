import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PraticesMessagesPage } from './pratices-messages.page';

const routes: Routes = [
  {
    path: '',
    component: PraticesMessagesPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PraticesMessagesPageRoutingModule {}
