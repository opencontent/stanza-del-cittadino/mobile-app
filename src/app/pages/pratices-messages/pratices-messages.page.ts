import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationsService } from '../../services/applications.service';
import { Messages } from '../../interfaces/applications';

@Component({
  selector: 'app-pratices-messages',
  templateUrl: './pratices-messages.page.html',
  styleUrls: ['./pratices-messages.page.scss'],
})
export class PraticesMessagesPage implements OnInit {
  private service: any;
  messages: Messages[];
  constructor(private route: ActivatedRoute, private router: Router, private applicationsService: ApplicationsService) {
    this.route.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.service = this.router.getCurrentNavigation().extras.state.detailPost;
        this.applicationsService.getApplicationsMessages(this.service.id).subscribe((res) => {
          this.messages = res;
        });
      }
    });
  }

  ngOnInit() {}
}
