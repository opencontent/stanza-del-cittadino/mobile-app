import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { StorageService } from '../../services/storage.service';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { AuthService } from '../../auth/auth.service';
import { Subject } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HelpPagePage } from '../../modals/help-page/help-page.page';
import { TranslateService } from '@ngx-translate/core';
import moment from 'moment';
const project = require('package.json');

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit, OnDestroy {
  token: string;
  comuni: any[];
  loading = true;
  error: any;
  profileForm: UntypedFormGroup;
  isSubmitted = false;
  items = [];
  showText = false;
  private unsubscribe$ = new Subject<void>();
  private newDataUser: any;
  appName: any = environment.appName;
  lastUpdate = environment.last_update

  constructor(
    private router: Router,
    private storageService: StorageService,
    public alertController: AlertController,
    public formBuilder: UntypedFormBuilder,
    public modalController: ModalController,
    private authService: AuthService,
    public loadingController: LoadingController,
    private translateService: TranslateService
  ) {}

  get errorControl() {
    return this.profileForm.controls;
  }

  ngOnInit() {}

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: '',
      header: 'Non sei abilitato a questa sezione',
      message: 'Per accedere al profilo devi essere <strong>registrato</strong> <br> effettua il login e riprova',
      buttons: [
        {
          text: this.translateService.instant('common.chiudi'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            this.router.navigate(['home']);
          },
        },
        {
          text: 'Login',
          handler: () => {
            this.router.navigate(['login']);
          },
        },
      ],
    });

    alert.onDidDismiss().then((data) => {
      this.showText = true;
    });

    await alert.present();
  }

  async logout() {
    await this.storageService
      .removeToken()
      .then(() => {
        setTimeout(() => {
          this.authService.isAuthenticated.next(false);
          this.router.navigateByUrl('/login');
        }, 500);
      })
      .catch(() => {
        this.authService.isAuthenticated.next(false);
        this.router.navigateByUrl('/login');
      });
  }

  async onSubmit() {
    this.isSubmitted = true;
    if (!this.profileForm.valid) {
      return false;
    } else {
      const loading = await this.loadingController.create({
        cssClass: '',
        message: this.translateService.instant('common.aggiornamenti_dati'),
      });
      this.newDataUser.phone = this.profileForm.value.phone;
    }
  }

  async presentAlertPost(header: string, message: string) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: [this.translateService.instant('common.chiudi')],
    });

    await alert.present();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  get version() {
    return `${project.version}`;
  }

  navigate(url: string, data?) {
    let navigationExtras: NavigationExtras = {
      state: {
        detailPost: data,
      },
    };
    this.router.navigate([url], navigationExtras);
  }

  async presentAlertLogout() {
    const alert = await this.alertController.create({
      cssClass: '',
      header: this.translateService.instant('logout.header'),
      message: this.translateService.instant('logout.message'),
      buttons: [
        {
          text: this.translateService.instant('common.annulla'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          },
        },
        {
          text: this.translateService.instant('common.esci'),
          handler: () => {
            console.log('Confirm Okay');
            this.logout();
          },
        },
      ],
    });
    alert.present();
  }

  async openHelpModal() {
    const modal = await this.modalController.create({
      component: HelpPagePage,
      cssClass: '',
      componentProps: {
        data:
          '<h3>Cosa puoi fare nel tuo profilo</h3>\n' +
          '<p><strong>Nella sezione profilo puoi gestire tutte le impostazioni.</strong></p>\n' +
          '<p>In particolare puoi gestire le tue preferenze e i dati anagrafici</p>\n' +
          '<p>&nbsp;</p>',
      },
    });
    return await modal.present();
  }
}
