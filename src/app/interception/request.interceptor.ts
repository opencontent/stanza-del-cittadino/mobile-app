import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders } from '@angular/common/http';
import { from, Observable } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { environment } from '../../environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthService, private translateService: TranslateService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with basic auth credentials if available
    if (request.url.includes(environment.api_stanza + '/api/')) {
      return from(this.handleAccess(request, next));
    }
    return next.handle(request);
  }

  private async handleAccess(req: HttpRequest<any>, next: HttpHandler): Promise<HttpEvent<any>> {
    let token = {
      value: null,
    };
    token = await this.authenticationService.getToken();
    const changedReq = req.clone({
      headers: new HttpHeaders({
        Authorization: `Bearer ${token.value}`,
        'x-locale': this.translateService.getDefaultLang() || 'it',
      }),
    });
    return next.handle(changedReq).toPromise();
  }
}
