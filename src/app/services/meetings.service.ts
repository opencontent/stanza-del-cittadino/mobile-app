import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class MeetingsService {
  constructor(private httpClient: HttpClient) {}

  getMeetings(): Observable<any> {
    return this.httpClient.get(`${environment.api_stanza}/api/meetings`);
  }

  getMeetingByID(id: string): Observable<any> {
    return this.httpClient.get(`${environment.api_stanza}/api/meetings/${id}`);
  }
}
